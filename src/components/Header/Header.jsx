import React from "react";
import PropTypes from "prop-types";

import { ReactComponent as Logo } from "./img/logo.svg";
import { ReactComponent as Favorites } from "./img/favorites.svg";
import { ReactComponent as Basket } from "./img/basket.svg";

import "./Header.scss";

export default class Header extends React.Component {
    render() {
        const { favoritesCounter, basketCounter } = this.props;
        return (
            <header className="header">
                <div className="header_wrapper">
                    <span className="header_logo">
                        <Logo />
                    </span>
                    <p className="header_name">Сторінка інтернет-магазину</p>
                    <div className="header_icons">
                        <span className="favorites">
                            <Favorites />
                            <span className="counter">{favoritesCounter}</span>
                        </span>
                        <span className="basket">
                            <Basket />
                            <span className="counter">{basketCounter}</span>
                        </span>
                    </div>
                </div>
            </header>
        );
    }
}

Header.propTypes = {
    favoritesCounter: PropTypes.number,
    basketCounter: PropTypes.number,
};
