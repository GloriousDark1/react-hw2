import React from "react";
import PropTypes from "prop-types";

import Button from "../../Button";
import { ReactComponent as Select } from "./img/select.svg";

import "./GoodsCard.scss";

export default class GoodsCard extends React.Component {
    state = { isSelected: false };

    // checkSelected = (id) => {
    //     const selected = JSON.parse(localStorage.getItem("favorites")) || [];
    //     return selected.some((elem) => elem.id === id);
    // };

    componentDidMount() {
        this.setState(() => ({
            isSelected: this.props.favoritesList.some(
                (elem) => elem.id === this.props.card.id
            ),
        }));
    }

    render() {
        const {
            card,
            handlerModal,
            addFavorites,
            removeFavorites,
            offerAddCard,
        } = this.props;
        const { name, price, image, article, color, id } = card;
        return (
            <div className="card" id={id}>
                <h4 className="card_title">{name}</h4>
                <img
                    className="card_img"
                    src={image}
                    alt={image}
                    width="250"
                    height="250"
                />
                <p className="card_article">Артикул: {article}</p>
                <p className="card_color">Колір - {color}</p>
                <p className="card_price">Ціна - {price} грн.</p>
                <div className="card_actions">
                    <Button
                        backgroundColor={"blue"}
                        text={"Додати в кошик"}
                        onClick={() => {
                            handlerModal();
                            offerAddCard(card);
                        }}
                    />
                    <span
                        className={
                            this.state.isSelected
                                ? "card_selected--active"
                                : "card_selected"
                        }
                        onClick={() => {
                            this.setState((prev) => ({
                                isSelected: !prev.isSelected,
                            }));
                            if (this.state.isSelected) {
                                removeFavorites(card);
                            } else {
                                addFavorites(card);
                            }
                        }}
                    >
                        <Select />
                    </span>
                </div>
            </div>
        );
    }
}

GoodsCard.propTypes = {
    key: PropTypes.number,
    card: PropTypes.object,
    handlerModal: PropTypes.func,
    addFavorites: PropTypes.func,
    removeFavorites: PropTypes.func,
    favoritesList: PropTypes.array,
    offerAddCard: PropTypes.func,
};

GoodsCard.defaultProps = {
    card: {},
    handlerModal: () => {},
    addFavorites: () => {},
    removeFavorites: () => {},
    favoritesList: [],
    offerAddCard: () => {},
};
